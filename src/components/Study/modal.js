import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'antd';

const NormalModal = ({ modalKey, visible, title, onCancel, res }) => {
  const data = { ...res };

  return (
    <div>
      <Modal key={modalKey} visible={visible} title={title} onCancel={onCancel}>
        <p>弹框测试{data.id}</p>
      </Modal>
      {/*<Button onClick={this.showModal} >打开</Button>*/}
    </div>
  );
};

// NormalModal.defaultProps = {
//   modalKey: 'modal',
//   visible: false,
//   title: 'info',
//   onCancel: '',
// };

NormalModal.propTypes = {
  modalKey: PropTypes.string,
  visible: PropTypes.bool,
  title: PropTypes.string,
  onCancel: PropTypes.func,
  res: PropTypes.object,
};

export default NormalModal;
