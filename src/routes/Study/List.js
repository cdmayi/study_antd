import React, { Component } from 'react';
import PageHeaderLayout from '../../layouts/PageHeaderLayout';
import { Divider, Table, Card } from 'antd';
import NormalModal from '../../components/Study/modal';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {
      editModalShow: false,
      editModalData: {},
    };
  }

  onCancel = () => {
    this.setState({
      editModalShow: false,
      editModalData: {},
    });
  };

  render() {
    const tableData = [
      { key: 1, id: 1, names: '张一', sex: 1, age: 23 },
      { key: 2, id: 2, names: '张二', sex: 1, age: 26 },
      { key: 3, id: 3, names: '张三', sex: 2, age: 18 },
      { key: 4, id: 4, names: '李四', sex: 2, age: 32 },
      { key: 5, id: 5, names: '王五', sex: 1, age: 50 },
    ];
    const tableHead = [
      { title: 'ID', dataIndex: 'id', key: 'id' },
      { title: '姓名', dataIndex: 'names', key: 'names' },
      {
        title: '性别',
        dataIndex: 'sex',
        key: 'sex',
        render(text, record) {
          switch (record.sex) {
            case 1:
              return '男';
              break;
            case 2:
              return '女';
              break;
            default:
          }
        },
      },
      { title: '年龄', dataIndex: 'age', key: 'age' },
      {
        title: '操作',
        dataIndex: 'action',
        key: 'action',
        render: (text, record) => {
          return (
            <span>
              <a
                href="javascript:void(0);"
                onClick={() => {
                  this.setState({
                    editModalShow: true,
                    editModalData: record,
                  });
                }}
              >
                修改
              </a>
              <Divider type="vertical" />
              <a href="javascript:void(0);">删除</a>
            </span>
          );
        },
      },
    ];

    return (
      <PageHeaderLayout>
        <Card>
          <Table columns={tableHead} dataSource={tableData} />
        </Card>

        <NormalModal
          modalKey="edit"
          visible={this.state.editModalShow}
          onCancel={this.onCancel}
          title="修改"
          res={this.state.editModalData}
        />
      </PageHeaderLayout>
    );
  }
}

export default List;
